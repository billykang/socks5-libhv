#ifndef CONN_H
#define CONN_H

#include "socks5.h"

#define DNS_MAXNAME 1024
struct  tunnel_remote_conn {
    char hostname[DNS_MAXNAME];
    uint16_t port;
    uint8_t addrtype;
    buffer_t *bndaddr;
};

struct tunnel_conn {
    struct socks5_server *server;
    hio_t *client;
    hio_t *remote;
        struct tunnel_remote_conn remote_conn;
#define SOCKS5_CONN_STAGE_UNINITIALIZED     0        
#define SOCKS5_CONN_STAGE_EXMETHOD          1
#define SOCKS5_CONN_STAGE_USERNAMEPASSWORD  2
#define SOCKS5_CONN_STAGE_EXHOST            3
#define SOCKS5_CONN_STAGE_DNSQUERY          4
#define SOCKS5_CONN_STAGE_CONNECTING        5
#define SOCKS5_CONN_STAGE_CONNECTED         6
#define SOCKS5_CONN_STAGE_STREAM            7
#define SOCKS5_CONN_STAGE_CLOSING           8
#define SOCKS5_CONN_STAGE_CLOSED            9
    uint8_t stage;
    uint8_t method;
    hloop_t *loop;
};

struct tunnel_conn *tunnel_conn_new();
void tunnel_conn_close(struct tunnel_conn *conn);
void tunnel_conn_setstage(struct tunnel_conn *conn, uint8_t stage);

#endif /* CONN_H */
