CC := gcc

MAIN_SRCS = $(wildcard src/*.c)
MAIN_OBJS = $(patsubst %.c,%.o,$(MAIN_SRCS))

# CFLAGS: 指定头文件（.h文件）的路径
# LDFLAGS: gcc等编译器会用到的一些优化参数，也可以在里面指定库文件的位置
# LIBS: 告诉链接器要链接哪些库文件

CFLAGS := -g -fPIC -Wall -O2
ifeq ($(debug), 1)
CFLAGS += -DDEBUG
endif
#LDFLAGS += -ldl -lrt -lpthread -lz -lsqlite3 -liconv -lcrypto -lblkid -lshared
#LDFLAGS += -ldl -lrt -lpthread -lz -lsqlite3 -liconv -lcrypto -lblkid -luuid -lshared -lgrnet -Wl,-rpath-link $(STAGING_DIR)/usr/lib

CFLAGS += -Isrc/ -Ilib/libhv/include -Ilib/buffer
CXXFLAGS := $(CFLAGS)
LDFLAGS := -Wl,-rpath,bin,-rpath, -L./ -lhv -lbuffer

ifneq ($(OS),Windows_NT)
  ifeq ($(shell uname),Darwin)
    CFLAGS += -framework CoreFoundation -framework CoreServices -framework Security
  endif
endif

#%.o : %.cpp
#	$(CXX) $(CFLAGS) $(CXXFLAGS) -c $< -o $@

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

all : libbuffer.a libhv.a proxy

proxy : $(MAIN_OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ libbuffer.a libhv.a -o proxy

#services : $(SERVICE_OBJS) $(MAIN_OBJS) $(API_DEPENDENCIES_C_OBJS)
#	$(CXX) -shared $(CFLAGS) $(LDFLAGS) $(SERVICE_OBJS) $(MAIN_OBJS) $(API_DEPENDENCIES_CPLUSPLUS_OBJS) $(API_DEPENDENCIES_C_OBJS) -o services.so
libhv.a:
	cd lib/libhv && ./configure && make
	cp lib/libhv/lib/libhv.a ./

libbuffer.a:
	cd lib/buffer && make libbuffer.a
	cp lib/buffer/libbuffer.a ./

clean : 
	rm -rf proxy
	rm -rf *.o src/*.o
	rm -rf *.so *.a
